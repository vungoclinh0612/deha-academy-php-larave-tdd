<?php

namespace Tests\Feature\Task;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Task;
use Illuminate\Http\Response;
use App\Models\User;

class getListTaskTest extends TestCase
{
    public function getListTaskRoute()
    {
        return route('tasks.index');
    }
    /**
     @test
     */
    public function unauthenticated_user_can_get_all_task(): void
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getListTaskRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task->name);
        
    }
    
    /**
     @test
     */
    public function unauthenticated_user_can_search_task_then_not_sort_task(): void
    {
        $task = Task::factory()->create();
        $searchTerm = $task->name;
        $response = $this->get('/tasks', ['search' => $searchTerm]);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task->name);
        $response->assertSee($task->content);
       

    }

     /**
     @test
     */
    public function unauthenticated_user_can_search_task_then_sort_task(): void
    {
        
        $task1 = Task::factory()->create(['name' => 'Task A']);
        $task2 = Task::factory()->create(['name' => 'Task B']);

        $response = $this->get('/tasks', ['search' => 'Task', 'sort' => 'asc']);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');

        // Kiểm tra xem cả hai công việc xuất hiện, và công việc thứ 1 (Task A) xuất hiện trước công việc thứ 2 (Task B)
        $response->assertSeeInOrder([$task1->name, $task2->name]);


    }
     /**
     @test
     */
    public function unauthenticated_user_can_sort_task () : void
    {

        $task1 = Task::factory()->create(['name' => 'Task A']);
        $task2 = Task::factory()->create(['name' => 'Task B']);

        $response = $this->get('/tasks', ['sort' => 'asc']);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');

        // Kiểm tra xem cả hai công việc xuất hiện, và công việc thứ 1 (Task A) xuất hiện trước công việc thứ 2 (Task B)
        $response->assertSeeInOrder([$task1->name, $task2->name]);
    }
     


}

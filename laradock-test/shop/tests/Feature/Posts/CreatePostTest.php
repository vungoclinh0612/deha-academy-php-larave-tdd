<?php

namespace Tests\Feature\Posts;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;


class CreatePostTest extends TestCase
{
    /**
     @test
     */
    public function user_can_create_post_if_data_is_valid(): void
    {
        $dataCreate = [
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
        ];
        $response = $this->Json('POST' , route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson (fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                    $json->where('title', $dataCreate['title'])
                ->etc()
            )->etc()
        );
        $this->assertDatabaseHas('posts',[
            'title' => $dataCreate['title'],
            'content' => $dataCreate['content'],
        ]);
    }

    /**
     @test
     */
    public function user_can_not_create_post_if_title_is_null ()
    {
        $dataCreate = [
            'title' => null,
            'content' => $this->faker->paragraph,
        ];
        $response = $this->Json('POST', route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson (fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('title')
                ->etc()
            )->etc()
        );
    }


    /**
     @test
     */
    public function user_can_not_create_post_if_content_is_null ()
    {
        $dataCreate = [
            'title' => $this->faker->sentence,
            'content' => null,
        ];
        $response = $this->Json('POST', route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson (fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('content')
                ->etc()
            )->etc()
        );
    }

    /**
     @test
     */
    public function user_can_not_create_post_if_data_is_not_valid ()
    {
        $dataCreate = [
            'title' => null,
            'content' => null,
        ];
        $response = $this->Json('POST', route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson (fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('content')
                ->has('title')
                ->etc()
            )->etc()
        );
    }
}

<?php

namespace Tests\Feature\Posts;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

class UpdatePostTest extends TestCase
{
    /**
     @test
     */
    public function user_can_update_post_when_post_exists_and_data_is_valid(): void
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
        ];
        $response = $this->Json('PUT', route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson (fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                $json->where('title', $dataUpdate['title'])
                ->etc()
            )->etc()
        );
        $this->assertDatabaseHas('posts',[
            'title' => $dataUpdate['title'],
            'content' => $dataUpdate['content'],
        ]);
        $response->assertStatus(200);
    }

     /**
     @test
     */
    public function user_can_not_update_post_if_post_exists_and_title_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'title' => null,
            'content' => $this->faker->paragraph,
        ];
        $response = $this->Json('PUT', route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson (fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('title')
                ->etc()
            )->etc()
        );
    }

     /**
     @test
     */
    public function user_can_not_update_post_if_post_exists_and_content_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'title' => $this->faker->sentence,
            'content' => null
        ];
        $response = $this->Json('PUT', route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson (fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('content')
                ->etc()
            )->etc()
        );
    }

    /**
     @test
     */
    public function user_can_not_update_post_if_post_exists_and_data_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'title' => null,
            'content' => null
        ];
        $response = $this->Json('PUT', route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson (fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('content')
                ->has('title')
                ->etc()
            )->etc()
        );
    }
 
     /**
     @test
     */
    public function user_can_not_update_post_if_post_not_exists_and_data_is_valid()
    {
        $post_id = -1;
        $dataUpdate = [
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
        ];
        $response = $this->Json('PUT', route('posts.update', $post_id), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}

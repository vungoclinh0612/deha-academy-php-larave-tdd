<?php

namespace Tests\Feature\Posts;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Task;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;


class GetPostTest extends TestCase
{
    /** @test */
    public function user_can_get_post_if_post_exists(): void
    {
        $task = Task::factory()->create();
        $response = $this->getJson(route('posts.show', $task->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson (fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                $json->where('title', $task->title)
                ->etc()
            )
            ->has('message')
            ->etc()
        );
        
    }
  /** @test */
    public function user_can_not_get_post_if_post_not_exists(): void
    {
        $post_id = -1;
        $response = $this->getJson(route('posts.show', $post_id));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}

<?php

namespace Tests\Feature\Posts;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;


class DeletePostTest extends TestCase
{
    /**
     @test
     */
    public function user_can_delete_post_if_post_exists(): void
    {
        $post = Post::factory()->create();
        $postCountBeforeDelete = Post::count();
        $response = $this->Json('DELETE', route('posts.destroy', $post->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson (fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                $json->where('title', $post->title)
                ->etc()
            )->etc()
        );
        $this->assertDatabaseMissing('posts',[
            'id' => $post->id,
            'title' => $post->title,
            'content' => $post->content,
        ]);
    }

    /**
     @test
     */
    public function user_can_not_delete_post_if_post_exists(): void
    {
        $post_id = -1;
        $response = $this->Json('DELETE', route('posts.destroy', $post_id));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

}

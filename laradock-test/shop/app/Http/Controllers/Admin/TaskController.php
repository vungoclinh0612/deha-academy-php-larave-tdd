<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Tasks\CreateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Response;


class TaskController extends Controller
{
    protected $result;
    protected $tasks;
    public function __construct(Task $tasks)
    {
        $this->tasks = $tasks;
    }

    public function sortTasks($tasks, $sortOption , $sortFieldText = 'name', $sortFieldTime = 'updated_at')
    {
        switch ($sortOption) {
            case 'asc':
                return $tasks->orderBy($sortFieldText, 'asc')->orderBy( $sortFieldTime, 'desc');
            case 'desc':
                return $tasks->orderBy($sortFieldText, 'desc')->orderBy($sortFieldTime, 'desc');
            case 'old':
                return $tasks->orderBy($sortFieldTime, 'asc')->orderBy($sortFieldText, 'asc') ;
            default:
                return $tasks->orderBy($sortFieldTime, 'desc')->orderBy($sortFieldText, 'asc');
        }
    }

    public function index(Request $request)
    {
        $sortOption = $request->input('sort', '');
        $searchTerm = $request->input('search', '');
       
        if ($request->has('search')) {
            $this->result['searchTerm'] = $searchTerm;

            $tasks = $this->tasks->where('name', 'like', '%'.$searchTerm.'%')
                    ->orWhere('content', 'like', '%'.$searchTerm.'%');
            $tasks = $this->sortTasks($tasks, $sortOption);        
                // if ($sortOption == 'asc') {
                //     $tasks= $tasks->orderBy('name', 'asc')->latest('updated_at');
                // }elseif ($sortOption == 'desc'){
                //     $tasks= $tasks->orderBy('name', 'desc')->latest('updated_at');
                // }elseif ($sortOption == 'late'){
                //     $tasks= $tasks->latest('updated_at')->orderBy('name', 'asc');
                // }else{
                //     $tasks= $tasks->oldest('updated_at')->orderBy('name', 'asc');
                // }
                
        } elseif ($request->has('sort')) {
            $tasks = $this->sortTasks($this->tasks , $sortOption);

            // if ($sortOption == 'asc') {
            //     $tasks= $this->task->orderBy('name', 'asc')->latest('updated_at');
            // } elseif ($sortOption == 'desc') {
            //     $tasks= $this->task->orderBy('name', 'desc')->latest('updated_at');
            // } elseif ($sortOption == 'late') {
            //     $tasks= $this->task->latest('updated_at')->orderBy('name', 'asc');
            // } else{
            //     $tasks= $this->task->oldest('updated_at')->orderBy('name', 'asc');
            // }

        } else{

            $tasks = $this->sortTasks($this->tasks, $sortOption);
        }
            $tasks= $tasks->paginate(10);
            $this->result['tasks'] = $tasks;
            return view('tasks.index',  ['data' => $this->result]);
    }
        

    public function create ()
    {
        return view('tasks.create');
    }

    public function store(CreateTaskRequest $request)
    {
        $this->tasks->create($request->all());
        return redirect()->route('tasks.index');
    }

    public function show($id)
    {
        $task = $this->tasks->findOrFail($id);
        return view('tasks.show', compact('task'));
    }

    public function edit($id)
    {
        $task = $this->tasks->findOrFail($id);
        return view('tasks.edit', compact('task'));
    }

    public function update(CreateTaskRequest $request ,$id)
    {
        $task = $this->tasks->find($id);
        $task->update($request->all());

        return redirect()->route('tasks.index');

    }

    public function destroy($id)
    { 
        $task = $this->tasks->findOrFail($id);
        $task->delete();
        return redirect()->route('tasks.index');
    }
}


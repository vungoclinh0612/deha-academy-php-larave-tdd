<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Resources\PostResource;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Http\Resources\PostCollection;
use Illuminate\Http\Response;

class PostController extends Controller
{
    protected $post;
    public function __construct(Post $post)
    {
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $posts = $this -> post -> paginate(5);
        $postResource = PostResource::collection ($posts)->response()->getData(true);
        return $this->sendSuccessResponse($postResource, Response::HTTP_OK,'success');

        // $posts = $this -> post -> paginate(5);
        // $postCollection = new PostCollection($posts);
        // return response()->json([
        //     'data' => $postCollection
        //     ], status : 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePostRequest $request)
    {
        $dataCreate = $request-> all();
        $post = $this -> post -> create($dataCreate);
        $postResource = new PostResource($post);
        return $this->sendSuccessResponse($postResource, Response::HTTP_OK,'success');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $post = $this -> post -> findOrFail($id);
        $postResource = new PostResource($post);
        return $this->sendSuccessResponse($postResource, Response::HTTP_OK,'success');
    }

    
    /**
     * Update the specified resource in storage.
     */
    public function update(StorePostRequest $request, string $id)
    {
        $post = $this -> post -> findOrFail($id);
        $dataUpdate = $request-> all();
        $post->update($dataUpdate);
        $postResource = new PostResource($post);
        return $this->sendSuccessResponse($postResource, Response::HTTP_OK,'success');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $post = $this -> post -> findOrFail($id);
        $post->delete();
        $postResource = new PostResource($post);
        return $this->sendSuccessResponse($postResource, Response::HTTP_OK,'delete success');
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4>Menu</h4>
                </div>

                <div class="card-body">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a href="{{ url('/tasks') }}"class="nav-link">Tasks</a>
                                </li>
                                
                            </ul>
                        </div>
                    </nav>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

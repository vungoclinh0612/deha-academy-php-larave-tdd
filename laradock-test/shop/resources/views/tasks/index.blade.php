@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        {{-- <div class="card-header">Dashboard</div> --}}

        <div class="card-body">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                    {{ session('status') }}
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Tasks</h4>
                            
                        </div>

                        <div class="card-body">

                          <div style="display: flex; justify-content: space-between; margin-bottom: 15px;">
                            <a href="javascript:history.back()"  class="btn btn-info float-left">
                              <i class="fas fa-arrow-left"></i> Back
                            </a>
                        
                            <a href="{{ route('tasks.create') }}" class="btn btn-success">
                                 Create <i class="fas fa-plus"></i>
                            </a>
                          </div>

                          <div style="display: flex; justify-content: space-between; margin-bottom: 15px;">
                            <div class="search-container" style="flex-grow: 1; display: flex; align-items: center;">
                              <div style=" width: 80%; "> <!-- Thay đổi ở đây -->
                                <input name="search" type="text" id="searchInput" placeholder="Search..." style="width: 100%; padding: 10px;"value="{{ isset($data['searchTerm']) ? $data['searchTerm'] : '' }}">
                              </div>
                        
                              <button type="button" onclick="search()" style="margin-left: 10px; background-color: #4CAF50; color: white; border: none; padding: 10px; border-radius: 5px;">
                                  Search
                              </button>
                              {{-- <button type="button" onclick="resetSearch()" style="margin-left: 10px; background-color: #ff0000; color: white; border: none; padding: 10px; border-radius: 5px;">
                                Reset
                            </button> --}}
                              <button style="background-color: #ff0000; color: white; border: none; padding: 10px; border-radius: 5px; margin-left:10px">
                                <a href="{{ route('tasks.index') }}" style="text-decoration: none; color: inherit;">
                                    Reset
                                </a>
                              </button>
                            </div>
                        
                            <div style="display: flex; justify-content: space-between; margin-top: 5px;">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Sort
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{ request()->fullUrlWithQuery(['sort' => 'asc']) }}">a->z</a>
                                        <a class="dropdown-item" href="{{ request()->fullUrlWithQuery(['sort' => 'desc']) }}">z->a</a>

                                        <a class="dropdown-item" href="{{ request()->fullUrlWithQuery(['sort' => 'old']) }}">oldest</a>
                                        <a class="dropdown-item" href="{{ request()->fullUrlWithQuery(['sort' => 'late']) }}">latest (default)</a>
                                    </div>
                                </div>
                        
                            </div>
                        </div>
                         
                          <div class="content-body" style="margin-top: 15px" >
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th scope ="col">ID</th>
                                  <th scope ="col">Name</th>
                                  <th scope ="col">Content</th>
                                  <th scope ="col">Phone</th>
                                  <th scope ="col">Updated_at</th>
                                  <th scope ="col">Action</th>
                                </tr>
                              </thead>
                              <tbody id="task-list">
                                @if ($data['tasks']->count() > 0)
                                  @foreach ($data['tasks'] as $task)
                                  <tr>
                                      <th scope="row">{{$task->id }}</th>
                                      <td>{{$task->name }}</td>
                                      <td>{{$task->content }}</td>
                                      <td>{{$task->phone}}</td>
                                      <td>{{$task->updated_at}}</td>
                                      <td>
                                        <div style="display: flex;">
                                          <a href="{{ route('tasks.show', ['id' => $task->id]) }}" class="btn btn-primary">
                                            <i class="fas fa-eye"></i> 
                                          </a>
                                          <a href="{{ route('tasks.edit', ['id' => $task->id]) }}" class="btn btn-warning" style="margin-left: 5px;">
                                            <i class="fas fa-edit"></i> 
                                          </a>
                                          <form method="POST" action="{{ route('tasks.destroy', ['id' => $task->id]) }}" onsubmit="return confirm('Are you sure you want to delete?')">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger" type="submit" style="margin-left: 5px;" data-toggle="tooltip" data-placement="top" title="Delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                          </form>
                                        </div>
                                      </td>
                                  </tr>
                                  @endforeach
                                @else
                                  <p>No tasks found.</p>
                                @endif
                              
                              </tbody>
                            </table>
                            {{ $data['tasks']->links() }}
                          </div>
                        </div>
                        

                    </div>
                  </div>
            </div>    
        </div> 
      </div> 
    </div> 
  </div> 
</div> 
<script>
  function search() {
      // Lấy giá trị từ ô nhập
      var searchTerm = document.getElementById('searchInput').value;

      // Lấy URL hiện tại
      var currentUrl = new URL(window.location.href);

      // Cập nhật hoặc thêm query parameter 'search'
      currentUrl.searchParams.set('search', searchTerm);

      // Chuyển hướng đến URL mới
      window.location.href = currentUrl.toString();
  }
  function resetSearch() {
        // Xóa giá trị của input search
        document.getElementById('searchInput').value = '';

        // Xóa các query trong URL (nếu có)
        var urlWithoutQuery = window.location.href.split('?')[0];
        history.pushState({}, document.title, urlWithoutQuery);
    }
</script>
@endsection










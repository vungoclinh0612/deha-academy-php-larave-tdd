@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        {{-- <div class="card-header">Dashboard</div> --}}

        <div class="card-body">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                    {{ session('status') }}
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Tasks</h4>
                            
                        </div>

                        <div class="card-body">

                          <div style="display: flex; justify-content: space-between; margin-bottom: 15px;">
                            <a href="javascript:history.back()"  class="btn btn-info float-left">
                              <i class="fas fa-arrow-left"></i> Back
                            </a>
                        
                            <a href="{{ route('tasks.create') }}" class="btn btn-success">
                                 Create <i class="fas fa-plus"></i>
                            </a>
                          </div>
                         
                          <div class="content-body" style="margin-top: 15px" >
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th scope ="col">ID</th>
                                  <th scope ="col">Name</th>
                                  <th scope ="col">Content</th>
                                  <th scope ="col">Phone</th>
                                  <th scope ="col">Action</th>
                                </tr>
                              </thead>
                              <tbody id="task-list">
                                @if ($task->count() > 0)
                                  
                                  <tr>
                                      <th scope="row">{{$task->id }}</th>
                                      <td>{{$task->name }}</td>
                                      <td>{{$task->content }}</td>
                                      <td>{{$task->phone}}</td>
                                      <td>
                                        <div style="display: flex;">
                                          <a href="{{ route('tasks.show', ['id' => $task->id]) }}" class="btn btn-primary">
                                            <i class="fas fa-eye"></i> 
                                          </a>
                                          <a href="{{ route('tasks.edit', ['id' => $task->id]) }}" class="btn btn-warning" style="margin-left: 5px;">
                                            <i class="fas fa-edit"></i> 
                                          </a>
                                          <form method="POST" action="{{ route('tasks.destroy', ['id' => $task->id]) }}" onsubmit="return confirm('Are you sure you want to delete?')">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger" type="submit" style="margin-left: 5px;" data-toggle="tooltip" data-placement="top" title="Delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                          </form>
                                        </div>
                                      </td>
                                  </tr>
                                  
                                @else
                                  <p>No tasks found.</p>
                                @endif
                              
                              </tbody>
                            </table>
                            
                          </div>
                        </div>
                        

                    </div>
                  </div>
            </div>    
        </div> 
      </div> 
    </div> 
  </div> 
</div> 
@endsection









@extends('layouts.app')

@section('content')
<div class = "container">
  <div class = "row justify-content-center">
    <div class = "col-md-8">
      <div class = "card">
        {{-- <div class = " =  card-header">Dashboacard</div> --}}

        <div class = "card-body">
            @if (session('status'))
                <div class = "alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class = "row">
                <div class = "col-md-12">
                    <div class = " =  card">
                        <div class = " =  card-header">
                            <h4>Edit Task</h4>
                            <a href="javascript:history.back()"  class="btn btn-info float-left">
                              <i class="fas fa-arrow-left"></i> Back
                            </a>
                        </div>

                        <div class = " =  card-body">
                          <div class = "col-md-8">
                            
                            <form action = "{{route('tasks.update',['id' => $task->id])}}" class = "form-horizontal" method="POST">
                                @method('PUT')
                                @csrf
                              <div class = "form-group row" style="margin-top: 10px;">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" value="{{ !empty(old('name')) ? old('name') : $task->name }}">
                                        
                                    </div>
                                @error('name')
                                    <span id = "name-error" class = "error text-danger" for = "name" style = "display:block;">
                                        {{$message}}
                                    </span>
                                @enderror
                              </div>
                              <div class = "form-group row" style="margin-top: 10px;">
                                <label for="content" class="col-sm-2 col-form-label">Content</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="content" value="{{ !empty(old('content')) ? old('content') : $task->content }}">
                                    </div>
                                @error('name')
                                    <span id = "name-error" class = "error text-danger" for = "content" style = "display:block;">
                                        {{$message}}
                                    </span>
                                @enderror
                              </div>
                              <div class = "form-group row" style="margin-top: 10px;">
                                <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                                <div class="col-sm-10">
                                    <input type="tel" class="form-control" name="phone" value="{{ !empty(old('phone')) ? old('phone') : $task->phone }}">
                                </div>
                                @error('phone')
                                    <span id="name-error" class = "error text-danger" for="phone" style="display:block;">
                                        {{$message}}
                                    </span>
                                @enderror
                              </div>
                              <div class = "form-group row" style="margin-left: 100px; margin-top:10px">
                                <button class = "btn btn-success">Submit</button>
                              </div>
                            </form>
                          </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div> 
      </div> 
    </div> 
  </div> 
</div> 
@endsection
